<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>JIPS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/app.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
    @if(Request::path() !== 'login')
        @include('layouts.navbar')
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidenav">
                    @include('layouts.side-nav')
                </div>    
                <div class="col-12 col-sm content">
                    @yield('content')
                </div>
            </div>
        </div>
    @elseif(Request::path() === 'login')
        <div class="container-fluid">
            @yield('content')
        </div>
    @endif
    
    <script src="../js/app.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script>
        $(window).scroll(function() {
            if ($(window).scrollTop() >= 100) {
                $('.sidenav > .row').css('padding-top', '30px');
            } else {
                $('.sidenav > .row').css('padding-top', '130px');
            }
        });
    </script>
</body>
</html>