<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="pb-2">
                    <div>Komen untuk:</div>
                    <div>Bahagian Khidmat Pengurusan</div>
                </div>
                <div>Catatan</div>
                <div class="text-container">
                    <textarea class="form-control" rows="10"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-dismiss="modal">
                    <span><img class="btn-icon" src="../img/kembali-white.svg" alt=""></span>
                    Kembali
                </button>
                <button class="btn btn-primary" type="button" data-dismiss="modal" data-toggle="modal" data-target="#saveModal">
                    <span><img class="btn-icon" src="../img/save-white.svg" alt=""></span>
                    Simpan
                </button>
            </div>
        </div>
    </div>
</div>