<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', function(){ return view('layouts.login'); })->name('login');
Route::get('/dashboard', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);
Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);

Route::prefix('kpi')->name('kpi.')->group(function () {
    Route::get('/laporan-pantauan', ['uses' => 'KpiController@pantauan', 'as' => 'pemantauan']);
    Route::get('/laporan-pantauan/print', ['uses' => 'KpiController@downloadPDF', 'as' => 'print']);
    Route::get('/penyelarasan', ['uses' => 'KpiController@penyelarasan', 'as' => 'penyelarasan']);
    Route::get('/penyerahan', ['uses' => 'KpiController@penyerahan', 'as' => 'penyerahan']);
    Route::get('/edit', ['uses' => 'KpiController@edit', 'as' => 'edit']);
    Route::get('/senarai', ['uses' => 'KpiController@senarai', 'as' => 'senarai']);
});

Route::prefix('ketua-jabatan')->name('ketua-jabatan.')->group(function () {
    Route::get('/dashboard', ['uses' => 'KetuaJabatanController@index', 'as' => 'dashboard']);
    Route::get('/penyelarasan', ['uses' => 'KetuaJabatanController@penyelarasan', 'as' => 'penyelarasan']);
});

Route::prefix('penyelaras')->name('penyelaras.')->group(function () {
    Route::get('/dashboard', ['uses' => 'PenyelarasController@index', 'as' => 'dashboard']);
    Route::get('/penyelarasan', ['uses' => 'PenyelarasController@penyelarasan', 'as' => 'penyelarasan']);
});

Route::prefix('pelulus')->name('pelulus.')->group(function () {
    Route::get('/dashboard', ['uses' => 'PelulusController@index', 'as' => 'dashboard']);
    Route::get('/penyerahan', ['uses' => 'PelulusController@penyerahan', 'as' => 'penyerahan']);
    Route::get('/senarai', ['uses' => 'PelulusController@senarai', 'as' => 'senarai']);
});
