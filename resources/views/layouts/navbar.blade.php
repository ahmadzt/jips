<div class="navigation">
    <nav class="navbar fixed-top">
        <div class="container-fluid nav-wrapper">
            <div class="row px-5 align-items-center">
                <div class="col-4">
                    <a class="navbar-brand" href="#">
                        <img src="../img/JataJohor.svg" alt="" style="width:80px">
                    </a>
                </div>
                <div class="col-8">
                    <span>Johor Indeks Prestasi Sistem</span>
                </div>
            </div>
        </div>
    </nav>
    <div style="margin-top: 42px !important"></div>
    <div class="container-fluid subnav">
        <div class="row py-3 px-4 align-items-center pegawai-kpi">
            <div class="col-3 col-sm col-md-1 text-right">
                <img src="../img/JataJohor.svg" alt="">
            </div>
            <div class="col-9 col-sm-5 pl-0">
                <span>Unit Korporat & Kualiti</span>
            </div>
            <div class="col-12 col-sm col-md-4 text-right px-0 d-none d-sm-block">
                <img class="avatar" src="../img/avatar.png" alt="">
            </div>
            <div class="col-6 col-sm col-md-1 name-info">
                <div class="name">Wahida Amalin</div>
                <div class="position">Pegawai KPI</div>
            </div>
            <div class="col-6 col-sm col-md-1 text-right">
                <a class="btn btn-primary btn-round" href="{{ route('login') }}">KELUAR</a>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>