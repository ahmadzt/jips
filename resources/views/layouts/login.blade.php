@extends('layouts.app')

@section('content')
    <div class="row full-height login-page justify-content-md-center align-items-center">
        <div class="col-md-4 p-5 mr-sm-3 ml-sm-3 mt-sm-3 mb-sm-0 m-md-0 login-wrapper">
            <div class="text-center">
                <div class="login-instruction">
                    Sila masukkan ID pengguna anda beserta kata laluan anda
                </div>
                <div class="login-input">
                    <img src="../img/profile_round.svg" alt="">
                    <input type="text">
                </div>
                <div class="login-input mb-4">
                        <img src="../img/key_1.svg" alt="">
                    <input type="password">
                </div>
                <div class="login-btn mb-3">
                    <a class="btn btn-primary" href="{{ route('dashboard') }}">LOG MASUK</a>
                </div>
                <div class="mb-3">
                    <small>Terlupa kata laluan anda?</small>
                <small>Klik di <a href="#">sini</a></small>
                </div>
                <div class="row login-ict">
                    <div class="col-5 p-0">
                        <img src="../img/Logo BST ICT.png" alt="">
                    </div>
                    <div class="col-7 p-0">
                        <span>Di selenggara oleh:</span>
                        <br>
                        <span>Bahagian Sains Teknologi & ICT Johor</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 p-5 mr-sm-3 ml-sm-3 mb-sm-3 m-md-0 login-wrapper">
            <div class="login-logo">
                <img src="../img/JataJohor.svg" alt="">
            </div>
            <div class="login-description">
                <div class="title my-4">Johor Indeks Prestasi Sistem</div>
                <div class="description">Portal Johor Indeks Prestasi Sistem adalah bagi pemantauan KPI jabatan-jabatan kerajaan seluruh negeri Johor oleh pihak SUK</div>
            </div>
        </div>
    </div>
@endsection