<!-- Modal -->
<div class="modal fade" id="laporanKpiModal" tabindex="-1" role="dialog" aria-labelledby="laporanKpiModalTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-a4" role="document">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-right">
                <a href="{{ route('kpi.print') }}" class="btn btn-primary">Muat turun PDF</a>
                </div>
            </div>
        </div>
        @include('modal.laporan-kpi-content')
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
    </div>
    </div>
</div>
</div>