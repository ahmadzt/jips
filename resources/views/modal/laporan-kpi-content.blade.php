<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="../../css/app.css" />
</head>
<body>
    <div class="container-fluid laporan">
        <div class="row modal-title">
            <div class="col text-center">
                <span class="nama">
                    Laporan Key Performance Indicator (KPI) 2018 <br>
                    Q1 (Januari - Mac)
                </span> 
            </div>
        </div>
        <hr>
        <div class="row modal-graph">
            <div class="col text-center">
                <i>GRAPH HERE</i>
            </div>
        </div>
        <hr>
        <div class="row text-center mb-5">
            <div class="col-12 mb-3">
                <img src="../img/upen.png" class="logo" alt="">
            </div>
            <div class="col-12">
                <span class="nama">Badan Kawal Selia Air Johor (BAKAJ)</span>
            </div>
        </div>
        <div class="row text-center mb-3 traffic">
            <div class="col-4 col-md-2 traffic-title">
                <div class="num">20</div>
                <div class="desc">KPI</div>
            </div>
            <div class="col-4 col-md-2 traffic-light">
                <div class="green">
                    <div class="num">20</div>
                </div>
                <span class="small">85% ke atas</span>
            </div>
            <div class="col-4 col-md-2 traffic-light">
                <div class="orange">
                    <div class="num">20</div>
                </div>
                <span class="small">50% - 85%</span>
            </div>
            <div class="col-4 col-md-2 traffic-light">
                <div class="red">
                    <div class="num">20</div>
                </div>
                <span class="small">50% ke bawah</span>
            </div>
            <div class="col-4 col-md-2 traffic-light">
                <div class="blue">
                    <div class="num">20</div>
                </div>
                <span class="small">Tiada status</span>
            </div>
            <div class="col-4 col-md-2 traffic-light">
                <div class="grey">
                    <div class="num">20</div>
                </div>
                <span class="small">Gugur/Tidak aktif</span>
            </div>
        </div>
        <div class="row text-center mb-3 traffic">
            <div class="col-4 col-md-2 traffic-title">
                <div class="num">20</div>
                <div class="desc">KPI</div>
            </div>
            <div class="col-4 col-md-2 traffic-light">
                <div class="green">
                    <div class="num">20</div>
                </div>
                <span class="small">85% ke atas</span>
            </div>
            <div class="col-4 col-md-2 traffic-light">
                <div class="orange">
                    <div class="num">20</div>
                </div>
                <span class="small">50% - 85%</span>
            </div>
            <div class="col-4 col-md-2 traffic-light">
                <div class="red">
                    <div class="num">20</div>
                </div>
                <span class="small">50% ke bawah</span>
            </div>
            <div class="col-4 col-md-2 traffic-light">
                <div class="blue">
                    <div class="num">20</div>
                </div>
                <span class="small">Tiada status</span>
            </div>
            <div class="col-4 col-md-2 traffic-light">
                <div class="grey">
                    <div class="num">20</div>
                </div>
                <span class="small">Gugur/Tidak aktif</span>
            </div>
        </div>
        <hr>
        <div class="row modal-table">
            <div class="col-12 fokus">
                <span>FOKUS 1: Menyediakan kemudahan Awam dan Infrastruktur</span>
            </div>
            <div class="col-12 objektif">
                <span>Objektif 1: Mengurus Tadbir Urus Air</span>
            </div>
            <div class="col-2 kpi-title">
                <span>KPI</span>
            </div>
            <div class="col-10 kpi-description">
                <span>Jumlah Pembinaan Telaga Tiub</span>
                <div class="kpi-achievement">
                    50%
                </div>
            </div>
            <div class="col-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sasaran</th>
                            <th>Pencapaian</th>
                            <th>Catatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                2 buah telaga di Kota Tinggi
                            </td>
                            <td>
                                Dalam Penyediaan Kertas Kerja untuk dibawa kepada Jawatankuasa Rundingan Harga Peringkat Negeri
                            </td>
                            <td>
                                Sebagaimana Lampiran
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-12">
                <table class="table table-laporan">
                    <thead>
                        <tr class="text-center">
                            <th scope="col-2">Inisiatif</th>
                            <th scope="col-2">Sasaran 2018</th>
                            <th scope="col-3">Pencapaian Sehingga Disember 2017</th>
                            <th scope="col-2">Kemajuan</th>
                            <th scope="col-2">Catatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">
                                Inisiatif 1-1: <br>
                                Mesyuarat / Perbincangan Awal
                            </td>
                            <td class="text-center">
                                Jan 2017
                            </td>
                            <td>
                                Selesai mesyuarat diadakan pada 9.1.2017
                            </td>
                            <td class="text-center">
                                <div class="kemajuan green">100%</div>
                            </td>
                            <td>
                                Tiada catatan buat masa ini
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                Inisiatif 1-1: <br>
                                Mesyuarat / Perbincangan Awal
                            </td>
                            <td class="text-center">
                                Jan 2017
                            </td>
                            <td>
                                Selesai mesyuarat diadakan pada 9.1.2017
                            </td>
                            <td class="text-center">
                                <div class="kemajuan orange">70%</div>
                            </td>
                            <td>
                                Tiada catatan buat masa ini
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</body>
</html>



