<div class="modal fade" id="backModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="p-5">
                    <div class="mb-3">KPI Kumpulan Pembangunan Berjaya Diserahkan untuk tindakan Unit Korporat & Kualiti</div>
                    
                    <button class="btn btn-primary" type="button" data-dismiss="modal">
                        <span><img class="btn-icon" src="../img/kembali-white.svg" alt=""></span>
                        Kembali
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>