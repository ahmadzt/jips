@extends('layouts.app')

@section('content')
    <div class="">
        <div class="heading">
            laporan pemantauan
        </div>
        <div class="my-3 text-right">
            <button class="btn btn-primary">
                <span><img class="btn-icon" src="../img/pdf-icon.svg" alt=""></span>
                Eksport PDF
            </button>
        </div>
        <div class="cards report-monitor">
            <div class="card-title container-fluid mb-3">
                <div class="row">
                    <div class="col">
                        SUK Pembangunan
                    </div>
                    <div class="col text-right">
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#laporanKpiModal">
                            <span><img class="btn-icon" src="../img/present-icon.svg" alt=""></span>
                            Bentang Laporan
                        </button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row traffic mb-3">
                    <div class="col-12 col-md-12 col-lg-4 mb-3 mb-lg-0 align-self-center">
                        Bahagian Khidmat Pengurusan
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="green">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="orange">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="red">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="blue">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="grey">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md text-center align-self-center">
                        <a href="#"><img class="small-btn" src="../img/eye.svg" alt=""></a>
                        <a href="#" data-toggle="modal" data-target="#commentModal"><img class="small-btn" src="../img/comment.svg" alt=""></a>
                    </div>
                    <div class="col-4"></div>
                </div>
                <div class="row traffic mb-3">
                    <div class="col-12 col-md-12 col-lg-4 mb-3 mb-lg-0 align-self-center">
                        Bahagian Khidmat Pengurusan
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="green">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="orange">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="red">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="blue">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="grey">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md text-center align-self-center">
                        <a href="#"><img class="small-btn" src="../img/eye.svg" alt=""></a>
                        <a href="#" data-toggle="modal" data-target="#commentModal"><img class="small-btn" src="../img/comment.svg" alt=""></a>
                    </div>
                    <div class="col-4"></div>
                </div>
                <div class="row traffic mb-3">
                    <div class="col-12 col-md-12 col-lg-4 mb-3 mb-lg-0 align-self-center">
                        Bahagian Khidmat Pengurusan
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="green">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="orange">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="red">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="blue">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="grey">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md text-center align-self-center">
                        <a href="#"><img class="small-btn" src="../img/eye.svg" alt=""></a>
                        <a href="#" data-toggle="modal" data-target="#commentModal"><img class="small-btn" src="../img/comment.svg" alt=""></a>
                    </div>
                    <div class="col-4"></div>
                </div>
            </div>
        </div>
        <div class="cards report-monitor">
            <div class="card-title container-fluid mb-3">
                <div class="row">
                    <div class="col">
                        SUK Pembangunan
                    </div>
                    <div class="col text-right">
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#laporanKpiModal">
                            <span><img class="btn-icon" src="../img/present-icon.svg" alt=""></span>
                            Bentang Laporan
                        </button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row traffic mb-3">
                    <div class="col-12 col-md-12 col-lg-4 mb-3 mb-lg-0 align-self-center">
                        Bahagian Khidmat Pengurusan
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="green">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="orange">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="red">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="blue">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="grey">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md text-center align-self-center">
                        <a href="#"><img class="small-btn" src="../img/eye.svg" alt=""></a>
                        <a href="#" data-toggle="modal" data-target="#commentModal"><img class="small-btn" src="../img/comment.svg" alt=""></a>
                    </div>
                    <div class="col-4"></div>
                </div>
                <div class="row traffic mb-3">
                    <div class="col-12 col-md-12 col-lg-4 mb-3 mb-lg-0 align-self-center">
                        Bahagian Khidmat Pengurusan
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="green">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="orange">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="red">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="blue">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="grey">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md text-center align-self-center">
                        <a href="#"><img class="small-btn" src="../img/eye.svg" alt=""></a>
                        <a href="#" data-toggle="modal" data-target="#commentModal"><img class="small-btn" src="../img/comment.svg" alt=""></a>
                    </div>
                    <div class="col-4"></div>
                </div>
                <div class="row traffic mb-3">
                    <div class="col-12 col-md-12 col-lg-4 mb-3 mb-lg-0 align-self-center">
                        Bahagian Khidmat Pengurusan
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="green">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="orange">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="red">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="blue">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="grey">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md text-center align-self-center">
                        <a href="#"><img class="small-btn" src="../img/eye.svg" alt=""></a>
                        <a href="#" data-toggle="modal" data-target="#commentModal"><img class="small-btn" src="../img/comment.svg" alt=""></a>
                    </div>
                    <div class="col-4"></div>
                </div>
            </div>
        </div>
        <div class="cards report-monitor">
            <div class="card-title container-fluid mb-3">
                <div class="row">
                    <div class="col">
                        SUK Pembangunan
                    </div>
                    <div class="col text-right">
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#laporanKpiModal">
                            <span><img class="btn-icon" src="../img/present-icon.svg" alt=""></span>
                            Bentang Laporan
                        </button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row traffic mb-3">
                    <div class="col-12 col-md-12 col-lg-4 mb-3 mb-lg-0 align-self-center">
                        Bahagian Khidmat Pengurusan
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="green">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="orange">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="red">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="blue">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="grey">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md text-center align-self-center">
                        <a href="#"><img class="small-btn" src="../img/eye.svg" alt=""></a>
                        <a href="#" data-toggle="modal" data-target="#commentModal"><img class="small-btn" src="../img/comment.svg" alt=""></a>
                    </div>
                    <div class="col-4"></div>
                </div>
                <div class="row traffic mb-3">
                    <div class="col-12 col-md-12 col-lg-4 mb-3 mb-lg-0 align-self-center">
                        Bahagian Khidmat Pengurusan
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="green">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="orange">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="red">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="blue">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="grey">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md text-center align-self-center">
                        <a href="#"><img class="small-btn" src="../img/eye.svg" alt=""></a>
                        <a href="#" data-toggle="modal" data-target="#commentModal"><img class="small-btn" src="../img/comment.svg" alt=""></a>
                    </div>
                    <div class="col-4"></div>
                </div>
                <div class="row traffic mb-3">
                    <div class="col-12 col-md-12 col-lg-4 mb-3 mb-lg-0 align-self-center">
                        Bahagian Khidmat Pengurusan
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="green">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="orange">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="red">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="blue">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md traffic-light">
                        <div class="grey">
                            <div class="num">25</div>
                        </div>
                    </div>
                    <div class="col-4 col-md text-center align-self-center">
                        <a href="#"><img class="small-btn" src="../img/eye.svg" alt=""></a>
                        <a href="#" data-toggle="modal" data-target="#commentModal"><img class="small-btn" src="../img/comment.svg" alt=""></a>
                    </div>
                    <div class="col-4"></div>
                </div>
            </div>
        </div>
        
    </div>    
@endsection

{{-- Modal here --}}
@include('modal.laporan-kpi')
@include('modal.komen')