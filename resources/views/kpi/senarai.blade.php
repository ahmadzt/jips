@extends('layouts.app')

@section('content')
    <div class="">
        <div class="heading">
            Senarai KPI
        </div>
        <div class="cards filter-list">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 mb-3">
                        <input type="text" class="form-control" placeholder="Nama KPI">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 col-md-1 text-md-right align-self-center">
                        <span>Dari tahun</span>
                    </div>
                    <div class="col-12 col-md-2">
                        <div class="select-container">
                            <img src="../img/calendar.svg" alt="">
                            <select name="" id="" class="form-control with-icon">
                                <option value="1">2017</option>
                                <option value="2">2016</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-1 text-md-right align-self-center">
                        <span>ke tahun</span>
                    </div>
                    <div class="col-12 col-md-2">
                        <div class="select-container">
                            <img src="../img/calendar.svg" alt="">
                            <select name="" id="" class="form-control with-icon">
                                <option value="1">2017</option>
                                <option value="2">2016</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-2 text-md-right align-self-center">
                        <span>untuk suku tahun</span>
                    </div>
                    <div class="col-12 col-md-2">
                        <div class="select-container">
                            <img src="../img/pie.svg" alt="">
                            <select name="" id="" class="form-control with-icon">
                                <option value="1">Q1</option>
                                <option value="2">Q2</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cards list-chart">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 btn-sw mb-3">
                        <ul class="nav nav-pills btn-chart mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-kpi-tab" data-toggle="pill" href="#pills-kpi" role="tab" aria-controls="pills-home" aria-selected="true">KPI</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-inisiatif-tab" data-toggle="pill" href="#pills-inisiatif" role="tab" aria-controls="pills-profile" aria-selected="false">Inisiatif</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 justify-content-center mb-3">
                        <span>NAMA KPI |</span>
                        <span><strong> Peratus Pelaksanaan Penyelenggaraan Kawasan Tadahan Empangan Layang Hulu dan Layang Hilir </strong></span>
                    </div>
                    <div class="col-12">
                        <div class="tab-content" id="pills-tabContent">
                            {{-- graph here --}}
                            <div class="tab-pane fade show active" id="pills-kpi" role="tabpanel" aria-labelledby="pills-kpi-tab">
                                {!! $kpiChart->render() !!}
                            </div>
                            <div class="tab-pane fade" id="pills-inisiatif" role="tabpanel" aria-labelledby="pills-inisiatif-tab">
                                {!! $inisiatifChart->render() !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-12">

                    </div>
                </div>
            </div>
        </div>
        <div class="cards list-main">
            <div class="container-fluid">
                <div class="row jusitfy-content-end mb-3">
                    <div class="col-3 d-none d-md-block"></div>
                    <div class="col flag-container">
                        <img src="../img/green-flag.svg" alt="">
                        <span>> 85%</span>
                    </div>
                    <div class="col flag-container">
                        <img src="../img/orange-flag.svg" alt="">
                        <span>50% - 85%</span>
                    </div>
                    <div class="col flag-container">
                        <img src="../img/red-flag.svg" alt="">
                        <span>< 50%</span>
                    </div>
                    <div class="col flag-container">
                        <img src="../img/blue-flag.svg" alt="">
                        <span>N/A</span>
                    </div>
                    <div class="col flag-container">
                        <img src="../img/grey-flag.svg" alt="">
                        <span>Tidak Aktif</span>
                    </div>
                    <div class="col flag-container">
                        <img src="../img/black-flag.svg" alt="">
                        <span>Draf</span>
                    </div>
                </div>
                <div class="row senarai-kpi">
                    <div class="col-12 senarai-items">
                        <div class="row">
                            <div class="col-12 col-sm-8">
                                <div>
                                    <span>ID : </span>
                                    <span>0001</span>
                                </div>
                                <div>
                                    <span>Fokus : </span>
                                    <span>Menyediakan kemudahan awam dan infrastruktur</span>
                                </div>
                                <div>
                                    <span>Objektif : </span>
                                    <span>Mengurus Tadbir Urus Air</span>
                                </div>
                                <div>
                                    <span>KPI : </span>
                                    <span>Jumlah Pembinaan Telaga Tiub</span>
                                </div>
                                <div>
                                    <span>Inisiatif : </span>
                                    <span>Kajian Geofizik sedang dilaksanakan oleh pihak Jabatan Mineral dan Geosains</span>
                                </div>
                            </div>
                            <div class="col-6 col-sm-2 align-self-center">
                                <div class="flag-number">
                                    <img src="../img/green-flag.svg" alt="">
                                    <span class="number">100%</span>
                                </div>
                                <span class="clearfix"></span>
                            </div>
                            <div class="col-6 col-sm-2 text-center status-container">
                                <div>Status:</div>
                                <div class="status">aktif</div>
                                <div class="btn-sw"><button class="btn btn-light">Gugur</button></div>
                                <div class="btn-sw"><button class="btn btn-light">Pindaan</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 senarai-items">
                        <div class="row">
                            <div class="col-12 col-sm-8">
                                <div>
                                    <span>ID : </span>
                                    <span>0001</span>
                                </div>
                                <div>
                                    <span>Fokus : </span>
                                    <span>Menyediakan kemudahan awam dan infrastruktur</span>
                                </div>
                                <div>
                                    <span>Objektif : </span>
                                    <span>Mengurus Tadbir Urus Air</span>
                                </div>
                                <div>
                                    <span>KPI : </span>
                                    <span>Jumlah Pembinaan Telaga Tiub</span>
                                </div>
                                <div>
                                    <span>Inisiatif : </span>
                                    <span>Kajian Geofizik sedang dilaksanakan oleh pihak Jabatan Mineral dan Geosains</span>
                                </div>
                            </div>
                            <div class="col-6 col-sm-2 align-self-center">
                                <div class="flag-number">
                                    <img src="../img/green-flag.svg" alt="">
                                    <span class="number">100%</span>
                                </div>
                                <span class="clearfix"></span>
                            </div>
                            <div class="col-6 col-sm-2 text-center status-container">
                                <div>Status:</div>
                                <div class="status">aktif</div>
                                <div class="btn-sw"><button class="btn btn-light">Gugur</button></div>
                                <div class="btn-sw"><button class="btn btn-light">Pindaan</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 senarai-items">
                        <div class="row">
                            <div class="col-12 col-sm-8">
                                <div>
                                    <span>ID : </span>
                                    <span>0001</span>
                                </div>
                                <div>
                                    <span>Fokus : </span>
                                    <span>Menyediakan kemudahan awam dan infrastruktur</span>
                                </div>
                                <div>
                                    <span>Objektif : </span>
                                    <span>Mengurus Tadbir Urus Air</span>
                                </div>
                                <div>
                                    <span>KPI : </span>
                                    <span>Jumlah Pembinaan Telaga Tiub</span>
                                </div>
                                <div>
                                    <span>Inisiatif : </span>
                                    <span>Kajian Geofizik sedang dilaksanakan oleh pihak Jabatan Mineral dan Geosains</span>
                                </div>
                            </div>
                            <div class="col-6 col-sm-2 align-self-center">
                                <div class="flag-number">
                                    <img src="../img/green-flag.svg" alt="">
                                    <span class="number">100%</span>
                                </div>
                                <span class="clearfix"></span>
                            </div>
                            <div class="col-6 col-sm-2 text-center status-container">
                                <div>Status:</div>
                                <div class="status">aktif</div>
                                <div class="btn-sw"><button class="btn btn-light">Gugur</button></div>
                                <div class="btn-sw"><button class="btn btn-light">Pindaan</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-right pt-3">
                        <span class="fas fa-chevron-left"></span>
                        <span class="">1</span>
                        <span class="">2</span>
                        <span class="">...</span>
                        <span class="">3</span>
                        <span class="fas fa-chevron-right"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection