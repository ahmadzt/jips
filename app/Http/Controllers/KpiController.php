<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class KpiController extends Controller
{
    public function index(){

    }

    public function pantauan(){
        return view('kpi.laporan-pantauan');
    }

    public function penyelarasan(){
        return view('kpi.penyelarasan');
    }

    public function penyerahan(){
        return view('kpi.penyerahan');
    }
    
    public function edit(){
        return view('kpi.edit');
    }
    
    public function senarai(){

        $options = [];
        $options['scales']['xAxes'][]['stacked'] = true;
        $options['scales']['yAxes'][]['stacked'] = true;

        $kpiChart = app()->chartjs
        ->name('KPIbarChart')
        ->type('horizontalBar')
        ->labels(['Label x'])
        ->datasets([
            [
                "label" => "KPI 1",
                'backgroundColor' => ['rgba(244, 67, 54, 1)'],
                'data' => [12]
            ],
            [
                "label" => "KPI 2",
                'backgroundColor' => ['rgba(48, 57, 137, 1)'],
                'data' => [69]
            ],
            [
                "label" => "KPI 3",
                'backgroundColor' => ['rgba(255, 99, 122, 1)'],
                'data' => [50]
            ]
        ])
        ->options([
            'barThickness' => 5
        ]);
        
        $inisiatifChart = app()->chartjs
        ->name('barChartTest')
        ->type('bar')
        ->labels(['Label x'])
        ->datasets([
            [
                'stack' => '1',
                "label" => "My Second dataset",
                'backgroundColor' => ['rgba(244, 67, 54, 1)'],
                'data' => [12]
            ],
            [
                'stack' => '1',
                "label" => "My First dataset",
                'backgroundColor' => ['rgba(48, 57, 137, 1)'],
                'data' => [69]
            ],
            [
                'stack' => '2',
                "label" => "My Third dataset",
                'backgroundColor' => ['rgba(255, 99, 122, 1)'],
                'data' => [50]
            ],
            [
                'stack' => '2',
                "label" => "My Fourth dataset",
                'backgroundColor' => ['rgba(255, 99, 200, 1)'],
                'data' => [32]
            ],
            [
                'stack' => '3',
                "label" => "My Third dataset",
                'backgroundColor' => ['rgba(255, 99, 150, 1)'],
                'data' => [50]
            ],
            [
                'stack' => '3',
                "label" => "My Fourth dataset",
                'backgroundColor' => ['rgba(255, 90, 310, 1)'],
                'data' => [32]
            ],
            [
                'stack' => '4',
                "label" => "My Third dataset",
                'backgroundColor' => ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
                'data' => [50]
            ],
            [
                'stack' => '4',
                "label" => "My Fourth dataset",
                'backgroundColor' => ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
                'data' => [32]
            ],
            [
                'stack' => '5',
                "label" => "My Third dataset",
                'backgroundColor' => ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
                'data' => [50]
            ],
            [
                'stack' => '5',
                "label" => "My Fourth dataset",
                'backgroundColor' => ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
                'data' => [32]
            ]
        ])
        ->options($options);

        return view('kpi.senarai', compact('inisiatifChart', 'kpiChart'));
    }

    public function downloadPDF(){
        $pdf = PDF::loadView('modal.laporan-kpi-content');
        return $pdf->download('invoice.pdf');
    }
}
