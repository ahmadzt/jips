@extends('layouts.app')

@section('content')
<div class="dashboard">
    <div class="heading">
        dashboard ukk

        <div class="btn-group btn-group-toggle btn-toggle-same-width float-right" data-toggle="buttons">
            <label class="btn btn-secondary active">
                <input type="radio" name="options" id="option1" autocomplete="off" checked> Pemantauan
            </label>
            <label class="btn btn-secondary">
                <input type="radio" name="options" id="option2" autocomplete="off">UKK
            </label>
        </div>
    </div>
    <div class="cards">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-3 col-md-2 align-self-center text-right">
                    Pilih tahun
                </div>
                <div class="col-3 col-md-2">
                    <div class="select-container">
                        <img src="../img/calendar.svg" alt="">
                        <select name="" id="" class="form-control with-icon">
                            <option value="1">2017</option>
                            <option value="2">2016</option>
                        </select>
                    </div>
                </div>
                <div class="col-3 col-md-2 align-self-center text-right">
                    untuk suku tahun
                </div>
                <div class="col-3 col-md-2">
                    <div class="select-container">
                        <img src="../img/pie.svg" alt="">
                        <select name="" id="" class="form-control with-icon">
                            <option value="1">Q1</option>
                            <option value="2">Q2</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cards">
        <div class="container-fluid traffic">
            <div class="row text-center mb-3">
                <div class="col-4 col-md-2 traffic-title">
                    <div class="num">20</div>
                    <div class="desc">KPI</div>
                </div>
                <div class="col-4 col-md-2 traffic-light">
                    <div class="green">
                        <div class="num">20</div>
                    </div>
                    <span class="small">85% ke atas</span>
                </div>
                <div class="col-4 col-md-2 traffic-light">
                    <div class="orange">
                        <div class="num">20</div>
                    </div>
                    <span class="small">50% - 85%</span>
                </div>
                <div class="col-4 col-md-2 traffic-light">
                    <div class="red">
                        <div class="num">20</div>
                    </div>
                    <span class="small">50% ke bawah</span>
                </div>
                <div class="col-4 col-md-2 traffic-light">
                    <div class="blue">
                        <div class="num">20</div>
                    </div>
                    <span class="small">Tiada status</span>
                </div>
                <div class="col-4 col-md-2 traffic-light">
                    <div class="grey">
                        <div class="num">20</div>
                    </div>
                    <span class="small">Gugur/Tidak aktif</span>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-4 col-md-2 traffic-title">
                    <div class="num">20</div>
                    <div class="desc">INISIATIF</div>
                </div>
                <div class="col-4 col-md-2 traffic-light">
                    <div class="green">
                        <div class="num">20</div>
                    </div>
                    <span class="small">85% ke atas</span>
                </div>
                <div class="col-4 col-md-2 traffic-light">
                    <div class="orange">
                        <div class="num">20</div>
                    </div>
                    <span class="small">50% - 85%</span>
                </div>
                <div class="col-4 col-md-2 traffic-light">
                    <div class="red">
                        <div class="num">20</div>
                    </div>
                    <span class="small">50% ke bawah</span>
                </div>
                <div class="col-4 col-md-2 traffic-light">
                    <div class="blue">
                        <div class="num">20</div>
                    </div>
                    <span class="small">Tiada status</span>
                </div>
                <div class="col-4 col-md-2 traffic-light">
                    <div class="grey">
                        <div class="num">20</div>
                    </div>
                    <span class="small">Gugur/Tidak aktif</span>
                </div>
            </div>
        </div>
    </div>
    <div class="cards circle-list">
        <div class="container-fluid">
            <div class="row">
                <div class="company-container"><a href="#" class="company-list"><span class="">Semua Jabatan</span></a><span class="clearfix"></span></div>
                <ul>
                    <li><div class="company-container"><a href="#" class="company-list"><img class="logo" src="../img/upen.png" alt=""></a><span class="clearfix"></span>UPEN</div></li>
                    <li><div class="company-container"><a href="#" class="company-list"><img class="logo" src="../img/JataJohor.svg" alt=""></a><span class="clearfix"></span>SUK Pengurusan</div></li>
                    <li><div class="company-container"><a href="#" class="company-list"><img class="logo" src="../img/upen.png" alt=""></a><span class="clearfix"></span>UPEN</div></li>
                    <li><div class="company-container"><a href="#" class="company-list"><img class="logo" src="../img/JataJohor.svg" alt=""></a><span class="clearfix"></span>SUK Pembangunan</div></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="cards tab">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="list-group tab-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home">Notifikasi</a>
                        <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile">tugasan hari ini</a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="list-home">
                            <div>
                                Bulan ini, Mac 2018
                            </div>
                            <div class="list-container">
                                <ul class="list">
                                    <li><span class="red"></span> KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="blue"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="red"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="blue"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="blue"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="red"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="red"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="blue"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="blue"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="blue"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="red"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="blue"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="blue"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                    <li><span class="blue"></span>Kumpulkan KPI dari jabatan-jabatan <span class="date">20 Mac</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="list-profile">2</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cards tab">
        <div class="card-title">
            <span>peratus pungutan kpi</span>
        </div>
        <div class="bar-canvas">
            {!! $chartjs->render() !!}
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="list-group tab-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#telah-hantar">Notifikasi</a>
                        <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#belum-hantar">tugasan hari ini</a>
                        <button class="btn btn-primary">Semak KPI</button>
                    </div>
                </div>
                <div class="col-12">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="telah-hantar">
                            <div class="table-container">
                                <table class="table table-striped">
                                    <thead>
                                        <tr class="text-center">
                                            <td>Jabatan</td>
                                            <td>Tarikh Diterima</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Badan Kawal Selia Air Johor (BAKAJ)</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Bahagian Perumahan Johor</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Bahagian Sains Teknologi $ ICT Johor</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Badan Kawal Selia Air Johor (BAKAJ)</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Bahagian Perumahan Johor</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Bahagian Sains Teknologi $ ICT Johor</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Badan Kawal Selia Air Johor (BAKAJ)</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Bahagian Perumahan Johor</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Bahagian Sains Teknologi $ ICT Johor</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Badan Kawal Selia Air Johor (BAKAJ)</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Bahagian Perumahan Johor</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                        <tr>
                                            <td>Bahagian Sains Teknologi $ ICT Johor</td>
                                            <td class="text-center">20 Mac</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="belum-hantar">2</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cards tab">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="list-group tab-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#telah-hantar">STATUS KPI</a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="telah-hantar">
                            <div class="table-container">
                                <table class="table">
                                    <thead>
                                        <tr class="d-flex">
                                            <th class="col-1">ID KPI</th>
                                            <th class="col-3">Nama KPI</th>
                                            <th class="col-2">Tarikh Status</th>
                                            <th class="col-2">Status</th>
                                            <th class="col-2">Tugas</th>
                                            <th class="col-2">ID Pengguna</th>
                                        </tr>
                                        <tr class="d-flex">
                                            <td class="col-1">
                                                <input type="text" class="form-control" placeholder="ID KPI">
                                            </td>
                                            <td class="col-3">
                                                <input type="text" class="form-control" placeholder="Nama KPI">
                                            </td>
                                            <td class="col-2">
                                                <input type="text" class="form-control" placeholder="Tarikh Status">
                                            </td>
                                            <td class="col-2">
                                                <select type="text" class="form-control">
                                                    <option></option>
                                                    <option>Menunggu Kelulusan</option>
                                                    <option>Sudah Dipulangkan</option>
                                                </select>
                                            </td>
                                            <td class="col-2">
                                                <select type="text" class="form-control">
                                                    <option></option>
                                                    <option>Luluskan</option>
                                                </select>
                                            </td>
                                            <td class="col-2">
                                                <input type="text" class="form-control" placeholder="ID Pengguna">
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="d-flex">
                                            <td class="col-1">00001</td>
                                            <td class="col-3">KPI 1: Jumlah Pembinaan Telaga Tiub</td>
                                            <td class="col-2">01/07/2018</td>
                                            <td class="col-2">Menunggu Kelulusan</td>
                                            <td class="col-2"><u>Luluskan</u></td>
                                            <td class="col-2">A-23456</td>
                                        </tr>
                                        <tr class="d-flex">
                                            <td class="col-1">00002</td>
                                            <td class="col-3">KPI 2 : Peratus Pelaksanaan Penyelenggaraan kawasan tadahan Empangan Layang Hulu dan Layang Hilir </td>
                                            <td class="col-2">01/07/2018</td>
                                            <td class="col-2">Menunggu Kelulusan</td>
                                            <td class="col-2"><u>Luluskan</u></td>
                                            <td class="col-2">A-23456</td>
                                        </tr>
                                        <tr class="d-flex">
                                            <td class="col-1">00003</td>
                                            <td class="col-3">KPI 3 : Peratus pelaksanaan Perkhidmatan Sewaan Peralatan Sistem Pemantauan Kualiti Air Sungai Johor) (5 Tahun)</td>
                                            <td class="col-2">01/07/2018</td>
                                            <td class="col-2">Menunggu Kelulusan</td>
                                            <td class="col-2"><u>Luluskan</u></td>
                                            <td class="col-2">A-23456</td>
                                        </tr>
                                        <tr class="d-flex">
                                            <td class="col-1">00004</td>
                                            <td class="col-3">KPI 4 : Peratus pelaksanaan pemetaan udara menggunakan Unmanned Aerial Vehicle (UAV) di Kawasan Tadahan Air Empangan Linggiu.</td>
                                            <td class="col-2">01/07/2018</td>
                                            <td class="col-2">Menunggu Kelulusan</td>
                                            <td class="col-2"><u>Luluskan</u></td>
                                            <td class="col-2">A-23456</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="belum-hantar">2</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
