<div class="modal fade" id="saveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="p-4">
                    <div class="green-tick mb-3">
                        <span><img src="../img/check-white.svg" alt=""></span>
                    </div>
                    <div class="text-center">
                        <div class="mb-2">
                            Catatan Berjaya Disimpan
                        </div>
                        <button class="btn btn-primary" type="button" data-dismiss="modal">
                            {{-- <span><img class="btn-icon" src="../img/save-white.svg" alt=""></span> --}}
                            OK
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>