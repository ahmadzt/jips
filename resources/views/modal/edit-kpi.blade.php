<!-- Modal -->
<div class="modal fade" id="laporanKpiModal" tabindex="-1" role="dialog" aria-labelledby="laporanKpiModalTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-a4" role="document">
    <div class="modal-content">
    <div class="modal-body">
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-12">
                    <div>Edit:</div>
                    <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Inisiatif</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group custom-group">
                        <label>Sasaran</label>
                        <span><img src="../img/info.svg" alt=""></span>
                        <div>
                            <input type="text" class="form-control">
                            <input type="text" class="form-control">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group custom-group">
                        <label>Pencapaian</label>
                        <span><img src="../img/info.svg" alt=""></span>
                        <div>
                            <select type="text" class="form-control">
                                <option value="Selesai">Selesai</option>
                            </select>
                            <input type="text" class="form-control">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Kemajuan</label>
                        <span><img src="../img/info.svg" alt=""></span>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Pemberat</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Catatan</label>
                        <textarea class="form-control" rows="7"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-dismiss="modal">
            <span><img class="btn-icon" src="../img/kembali-white.svg" alt=""></span>
            Kembali
        </button>
        <button class="btn btn-primary" type="button" data-dismiss="modal">
            <span><img class="btn-icon" src="../img/save-white.svg" alt=""></span>
            Simpan
        </button>
    </div>
    </div>
</div>
</div>