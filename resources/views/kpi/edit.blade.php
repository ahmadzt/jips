@extends('layouts.app')

@section('content')
    <div class="">
        <div class="heading">
            laporan pemantauan
        </div>
        <div class="my-3 text-right">
            <button class="btn btn-primary">
                <span><img class="btn-icon" src="../img/eraser.svg" alt=""></span>
                Kosongkan Draf
            </button>
            <button class="btn btn-primary">
                <span><img class="btn-icon" src="../img/save.svg" alt=""></span>
                Simpan Draf
            </button>
        </div>
        <div class="edit-box">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 d-none d-md-block">
                        <button class="new-btn">
                            <div>KPI 1:</div>
                            <span>Jumlah Pembinaan Telaga Tiub</span>
                        </button>
                        <button class="new-btn active">
                            <div>KPI 2:</div>
                            <span>Jumlah Pembinaan Telaga Tiub</span>
                        </button>
                        <button class="new-btn">
                            <div>KPI 3:</div>
                            <span>Jumlah Pembinaan Telaga Tiub</span>
                        </button>
                        <button class="new-btn align-items-center justify-content-center">
                            <img class="" src="../img/plus-black.svg" alt="">
                        </button>
                    </div>
                    <div class="col edit-wrapper">

                        {{-- Fokus & Objektif --}}
                        <div class="row">
                            <div class="col">
                                <div class="row edit-form">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col">
                                                <label>Fokus</label>
                                                <select class="form-control">
                                                    <option>Fokus 1</option>
                                                    <option>Fokus 2</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <label>Objektif</label>
                                                <textarea class="form-control" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3 edit-comment">
                                <div class="row h-100 align-items-center text-center">
                                    <div class="col">
                                        <span><a href="#"><u>Lihat Komen</u></a></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- KPI --}}
                        <div class="row">
                            <div class="col">
                                <div class="row py-4 m-0">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <label>KPI</label>
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label>Muat Naik Lampiran</label>
                                                <div class="input-group-2 mb-3">
                                                    <input type="text" class="form-control input-group-item">
                                                    <button class="btn btn-primary input-group-item" type="button">Cari Fail</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <label>Sasaran</label>
                                                <input class="form-control mb-3" type="text">
                                                <label>Pencapaian</label>
                                                <div class="input-group-2 mb-3">
                                                    <input type="text" class="form-control input-group-item">
                                                    <button class="btn btn-outline-primary input-group-item" type="button">Unit</button>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label>Catatan</label>
                                                <textarea class="form-control" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3 edit-comment">
                                <div class="row h-100 align-items-center text-center">
                                    <div class="col">
                                        <span><a href="#"><u>Lihat Komen</u></a></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Inisiatif --}}
                        <div class="row">
                            <div class="col">
                                <div class="row py-4 m-0">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <label>Inisiatif</label>
                                                <input class="form-control mb-3" type="text">
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label>Pemberat</label>
                                                <input type="text" class="form-control mb-3">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <label>Sasaran</label>
                                                <div class="input-group-2 mb-3">
                                                    <input type="text" class="form-control input-group-item">
                                                    <input type="text" class="form-control input-group-item">
                                                    <button class="btn btn-outline-primary input-group-item" type="button">Unit</button>
                                                </div>
                                                <label>Pencapaian</label>
                                                <div class="input-group-2 mb-3">
                                                    <select type="text" class="form-control input-group-item">
                                                        <option>1</option>
                                                        <option>2</option>
                                                    </select>
                                                    <input type="text" class="form-control input-group-item">
                                                    <button class="btn btn-outline-primary input-group-item" type="button">Unit</button>
                                                </div>
                                                <label>Kemajuan</label>
                                                <input type="text" class="form-control mb-3">
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label>Catatan</label>
                                                <textarea class="form-control" rows="5"></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col text-right">
                                                <button class="btn btn-primary">
                                                    <span><img class="btn-icon" src="../img/delete.svg" alt=""></span>
                                                    Buang Inisiatif
                                                </button>
                                                <button class="btn btn-primary">
                                                        <span><img class="btn-icon" src="../img/plus.svg" alt=""></span>
                                                    Tambah Inisiatif
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3 edit-comment border-0">
                                <div class="row h-100 align-items-center text-center">
                                    <div class="col">
                                        <span><a href="#"><u>Lihat Komen</u></a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="my-3 text-right">
            <button class="btn btn-primary">
                <span><img class="btn-icon" src="../img/eraser.svg" alt=""></span>
                Kosongkan Draf
            </button>
            <button class="btn btn-primary">
                <span><img class="btn-icon" src="../img/save.svg" alt=""></span>
                Simpan Draf
            </button>
        </div>
    </div>
@endsection