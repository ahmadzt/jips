<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KetuaJabatanController extends Controller
{
    public function index(){
        $chartjs = app()->chartjs
        ->name('barChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 30])
        ->labels(['Label x', 'Label y'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [69, 59]
            ]
        ])
        ->options([])
        ->optionsRaw([
            'legend' => [
                'display' => true,
                'position' => 'right'
            ],
            'tooltips' => [
                'enabled' => true,
                'mode' => 'index'
            ]
        ]);

        return view('dashboard.ukk', compact('chartjs'));
    }

    public function penyelarasan(){
        return view('kpi.penyelarasan');
    }
}
