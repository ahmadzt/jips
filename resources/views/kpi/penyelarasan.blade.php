@extends('layouts.app')

@section('content')
    <div>
        <div class="heading">
            penyelarasan kpi
        </div>
        <div class="sub-heading">
            Badan Kawal Selia Air Johor (BAKAJ)
        </div>
        <div class="container-fluid mb-3">
            <div class="row">
                <div class="col p-0">
                    <button class="btn btn-primary mb-2" data-toggle="modal" data-target="#backModal">
                        <span><img class="btn-icon" src="../img/save.svg" alt=""></span>
                        Serah KPI
                    </button>
                </div>
                <div class="col text-sm-right p-0">
                    <button class="btn btn-primary mb-2">
                        <span><img class="btn-icon" src="../img/pdf-icon.svg" alt=""></span>
                        Eksport Semua Jabatan
                    </button>
                    <button class="btn btn-primary mb-2" data-toggle="modal" data-target="#laporanKpiModal" data-toggle="modal" data-target="#laporanKpiModal">
                        <span><img class="btn-icon" src="../img/pdf-icon.svg" alt=""></span>
                        Eksport PDF
                    </button>
                </div>
            </div>
        </div>
        <div class="cards penyelarasan">
            <div> {{-- @foreach here --}}
                <div class="container-fluid">
                    <div class="row">
                        <div class="fokus-header">
                            <span>Fokus 1: Menyediakan Kemudahan Awam Dan Infrastruktur</span>
                            <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                        </div>
                        <div class="objektif-header">
                            <span>Objektif 1: Mengurus Tadbir Urus Air</span>
                            <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col parent-node ml-0 ml-sm-4">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Lampiran:</div>
                                        <ul>
                                            <li><a href="#"> Minit_1</a></li>
                                            <li><a href="#"> Minit_2</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan orange">
                                                    50%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col parent-node ml-0 ml-sm-4">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Lampiran:</div>
                                        <ul>
                                            <li><a href="#"> Minit_1</a></li>
                                            <li><a href="#"> Minit_2</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan orange">
                                                    50%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                            </div>
                        </div>
                        <div class="row inisiatif">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                            </div>
                        </div>
                        <div class="row inisiatif">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col parent-node ml-0 ml-sm-4">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Lampiran:</div>
                                        <ul>
                                            <li><a href="#"> Minit_1</a></li>
                                            <li><a href="#"> Minit_2</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan orange">
                                                    50%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> {{-- @endforeach here --}}
            <div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="fokus-header">
                            <span>Fokus 2: Menyediakan Kemudahan Awam Dan Infrastruktur</span>
                        </div>
                        <div class="objektif-header">
                            <span>Objektif 1: Mengurus Tadbir Urus Air</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col parent-node ml-0 ml-sm-4">
                            <div class="row">
                                <div class="col-12 col-md-8">
                                    <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                    <div>Sasaran: Jan 2018</div>
                                    <div>Lampiran:</div>
                                    <ul>
                                        <li><a href="#"> Minit_1</a></li>
                                        <li><a href="#"> Minit_2</a></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="row justify-content-md-center">
                                        <div class="col mt-2 mt-md-0">
                                            <div>Kemajuan:</div>
                                            <div class="kemajuan orange">
                                                50%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col child-node ml-0 ml-sm-5">
                            <div class="row">
                                <div class="col-12 col-md-8">
                                    <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                    <div>Sasaran: Jan 2018</div>
                                    <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                    <div>Kemajuan: 100%</div>
                                    <div>Status: Aktif</div>
                                    <div>Catatan: Tiada catatan buat masa kini</div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="row justify-content-md-center">
                                        <div class="col mt-2 mt-md-0">
                                            <div>Kemajuan:</div>
                                            <div class="kemajuan green">
                                                100%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="edit-icon"><img src="../img/pencil.svg" alt=""></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col p-0">
                    <button class="btn btn-primary mb-2" data-toggle="modal" data-target="#backModal">
                        <span><img class="btn-icon" src="../img/save.svg" alt=""></span>
                        Serah KPI
                    </button>
                </div>
                <div class="col text-sm-right p-0">
                    <button class="btn btn-primary mb-2">
                        <span><img class="btn-icon" src="../img/pdf-icon.svg" alt=""></span>
                        Eksport Semua Jabatan
                    </button>
                    <button class="btn btn-primary mb-2" data-toggle="modal" data-target="#laporanKpiModal">
                        <span><img class="btn-icon" src="../img/pdf-icon.svg" alt=""></span>
                        Eksport PDF
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('modal.kembali')
@include('modal.laporan-kpi')