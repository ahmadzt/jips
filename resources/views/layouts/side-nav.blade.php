<div class="row">
    <div class="col-12 selected">
        <a href="{{ route('dashboard') }}">
            <div class="text">dashboard</div>
            <img src="../img/dashboard-blue.svg" alt="">
        </a>
    </div>
    <div class="col-12">
        <a href="{{ route('kpi.senarai') }}">
            <div class="text">senarai kpi</div>
            <img src="../img/senarai-kpi-white.svg" alt="">
        </a>
    </div>
    <div class="col-12">
        <a href="{{ route('kpi.edit') }}">
            <div class="text">edit kpi</div>
            <img src="../img/edit-kpi-white.svg" alt="">
        </a>
    </div>
    <div class="col-12">
        <a href="{{ route('kpi.penyerahan') }}">
            <div class="text">serah kpi</div>
            <img src="../img/serah-kpi-white.svg" alt="">
        </a>
    </div>
    <div class="col-12">
        <div class="text">profil</div>
        <img src="../img/profile-white.svg" alt="">
    </div>
    <div class="col-12">
        <div class="text">tetapan</div>
        <img src="../img/setting-white.svg" alt="">
    </div>
</div>