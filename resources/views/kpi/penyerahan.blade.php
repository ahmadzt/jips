@extends('layouts.app')

@section('content')
    <div>
        <div class="heading">
            serah kpi
        </div>
        <div class="container-fluid mb-3">
            <div class="row">
                <div class="col p-0">
                    <button class="btn btn-primary mb-2">
                        <span><img class="btn-icon" src="../img/eye.png" alt=""></span>
                        Lihat Komen
                    </button>
                </div>
                <div class="col text-sm-right p-0">
                    <button class="btn btn-primary mb-2" data-toggle="modal" data-target="#laporanKpiModal">
                        <span><img class="btn-icon" src="../img/pdf-icon.svg" alt=""></span>
                        Eksport PDF
                    </button>
                    <button class="btn btn-primary mb-2">
                        <span><img class="btn-icon" src="../img/pdf-icon.svg" alt=""></span>
                        Pulang KPI
                    </button>
                    <button class="btn btn-outline-primary mb-2">
                        <span><img class="btn-icon" src="../img/check.svg" alt=""></span>
                        Serah KPI
                    </button>
                    <button class="btn btn-success mb-2">
                        Lulus Semua
                    </button>
                </div>
            </div>
        </div>
        <div class="cards">
            <div class="container-fluid">
                <div class="row">
                    <div class="col p-0">
                        <div class="mb-2">Komen dari Unit Korporat dan Kualiti</div>
                        <textarea class="form-control" id="" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="cards treeview">
            <ul class="p-0">
                <li>
                    {{-- fokus --}}
                    <div class="d-flex">
                        <div class="flex-grow fokus-header" data-toggle="collapse" data-target="#collapse-1">
                            <span>Fokus 1: Menyediakan Kemudahan Awam Dan Infrastruktur</span>
                        </div>
                        <div class="flex-grow switch-box">
                            <div class="row">
                                <div class="col align-self-center text-center">
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                    <div class="switch-text">
                                        <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- objektif --}}
                    <div class="d-flex">
                        <div class="flex-grow objektif-header">
                            <span>Objektif 1: Mengurus Tadbir Urus Air</span>
                        </div>
                        <div class="flex-grow switch-box">
                            <div class="row">
                                <div class="col align-self-center text-center">
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                    <div class="switch-text">
                                        <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- kpi --}}
                    <ul class="tree collapse" id="collapse-1">
                        <li>
                            <div class="d-flex tree-v">
                                <div class="flex-grow objektif-header border" data-toggle="collapse" data-target="#collapse-1-1">
                                    <div class="row">
                                        <div class="col-12 col-md-8">
                                            <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                            <div>Sasaran: Jan 2018</div>
                                            <div>Lampiran:</div>
                                            <ul class="lampiran">
                                                <li><a href="#"> Minit_1</a></li>
                                                <li><a href="#"> Minit_2</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <div class="row justify-content-md-center">
                                                <div class="col mt-2 mt-md-0">
                                                    <div>Kemajuan:</div>
                                                    <div class="kemajuan orange">
                                                        50%
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex-grow switch-box">
                                    <div class="row">
                                        <div class="col align-self-center text-center">
                                            <label class="switch">
                                                <input type="checkbox">
                                                <div class="switch-btn"></div>
                                            </label>
                                            <div class="switch-text">
                                                <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- inisiatif --}}
                            <ul class="tree collapse" id="collapse-1-1">
                                <li>
                                    <div class="d-flex tree-v">
                                        <div class="flex-grow objektif-header border bg-white">
                                            <div class="row">
                                                <div class="col-12 col-md-8">
                                                    <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                                    <div>Sasaran: Jan 2018</div>
                                                    <div>Lampiran:</div>
                                                    <ul class="lampiran">
                                                        <li><a href="#"> Minit_1</a></li>
                                                        <li><a href="#"> Minit_2</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <div class="row justify-content-md-center">
                                                        <div class="col mt-2 mt-md-0">
                                                            <div>Kemajuan:</div>
                                                            <div class="kemajuan orange">
                                                                50%
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-grow switch-box">
                                            <div class="row">
                                                <div class="col align-self-center text-center">
                                                    <label class="switch">
                                                        <input type="checkbox">
                                                        <div class="switch-btn"></div>
                                                    </label>
                                                    <div class="switch-text">
                                                        <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex tree-v">
                                        <div class="flex-grow objektif-header border bg-white">
                                            <div class="row">
                                                <div class="col-12 col-md-8">
                                                    <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                                    <div>Sasaran: Jan 2018</div>
                                                    <div>Lampiran:</div>
                                                    <ul class="lampiran">
                                                        <li><a href="#"> Minit_1</a></li>
                                                        <li><a href="#"> Minit_2</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <div class="row justify-content-md-center">
                                                        <div class="col mt-2 mt-md-0">
                                                            <div>Kemajuan:</div>
                                                            <div class="kemajuan orange">
                                                                50%
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-grow switch-box">
                                            <div class="row">
                                                <div class="col align-self-center text-center">
                                                    <label class="switch">
                                                        <input type="checkbox">
                                                        <div class="switch-btn"></div>
                                                    </label>
                                                    <div class="switch-text">
                                                        <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <div class="d-flex tree-v">
                                <div class="flex-grow objektif-header border">
                                    <div class="row">
                                        <div class="col-12 col-md-8">
                                            <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                            <div>Sasaran: Jan 2018</div>
                                            <div>Lampiran:</div>
                                            <ul class="lampiran">
                                                <li><a href="#"> Minit_1</a></li>
                                                <li><a href="#"> Minit_2</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <div class="row justify-content-md-center">
                                                <div class="col mt-2 mt-md-0">
                                                    <div>Kemajuan:</div>
                                                    <div class="kemajuan orange">
                                                        50%
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex-grow switch-box">
                                    <div class="row">
                                        <div class="col align-self-center text-center">
                                            <label class="switch">
                                                <input type="checkbox">
                                                <div class="switch-btn"></div>
                                            </label>
                                            <div class="switch-text">
                                                <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- inisiatif --}}
                            <ul>
                                <li>
                                    <div class="d-flex tree-v">
                                        <div class="flex-grow objektif-header border bg-white">
                                            <div class="row">
                                                <div class="col-12 col-md-8">
                                                    <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                                    <div>Sasaran: Jan 2018</div>
                                                    <div>Lampiran:</div>
                                                    <ul class="lampiran">
                                                        <li><a href="#"> Minit_1</a></li>
                                                        <li><a href="#"> Minit_2</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <div class="row justify-content-md-center">
                                                        <div class="col mt-2 mt-md-0">
                                                            <div>Kemajuan:</div>
                                                            <div class="kemajuan orange">
                                                                50%
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-grow switch-box">
                                            <div class="row">
                                                <div class="col align-self-center text-center">
                                                    <label class="switch">
                                                        <input type="checkbox">
                                                        <div class="switch-btn"></div>
                                                    </label>
                                                    <div class="switch-text">
                                                        <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex tree-v">
                                        <div class="flex-grow objektif-header border bg-white">
                                            <div class="row">
                                                <div class="col-12 col-md-8">
                                                    <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                                    <div>Sasaran: Jan 2018</div>
                                                    <div>Lampiran:</div>
                                                    <ul class="lampiran">
                                                        <li><a href="#"> Minit_1</a></li>
                                                        <li><a href="#"> Minit_2</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <div class="row justify-content-md-center">
                                                        <div class="col mt-2 mt-md-0">
                                                            <div>Kemajuan:</div>
                                                            <div class="kemajuan orange">
                                                                50%
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-grow switch-box">
                                            <div class="row">
                                                <div class="col align-self-center text-center">
                                                    <label class="switch">
                                                        <input type="checkbox">
                                                        <div class="switch-btn"></div>
                                                    </label>
                                                    <div class="switch-text">
                                                        <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    {{-- fokus --}}
                    <div class="d-flex">
                        <div class="flex-grow fokus-header">
                            <span>Fokus 1: Menyediakan Kemudahan Awam Dan Infrastruktur</span>
                        </div>
                        <div class="flex-grow switch-box">
                            <div class="row">
                                <div class="col align-self-center text-center">
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                    <div class="switch-text">
                                        <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- objektif --}}
                    <div class="d-flex">
                        <div class="flex-grow objektif-header">
                            <span>Objektif 1: Mengurus Tadbir Urus Air</span>
                        </div>
                        <div class="flex-grow switch-box">
                            <div class="row">
                                <div class="col align-self-center text-center">
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                    <div class="switch-text">
                                        <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- kpi --}}
                    <ul>
                        <li>
                            <div class="d-flex">
                                <div class="flex-grow objektif-header border">
                                    <div class="row">
                                        <div class="col-12 col-md-8">
                                            <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                            <div>Sasaran: Jan 2018</div>
                                            <div>Lampiran:</div>
                                            <ul class="lampiran">
                                                <li><a href="#"> Minit_1</a></li>
                                                <li><a href="#"> Minit_2</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <div class="row justify-content-md-center">
                                                <div class="col mt-2 mt-md-0">
                                                    <div>Kemajuan:</div>
                                                    <div class="kemajuan orange">
                                                        50%
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex-grow switch-box">
                                    <div class="row">
                                        <div class="col align-self-center text-center">
                                            <label class="switch">
                                                <input type="checkbox">
                                                <div class="switch-btn"></div>
                                            </label>
                                            <div class="switch-text">
                                                <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- inisiatif --}}
                            <ul>
                                <li>
                                    <div class="d-flex">
                                        <div class="flex-grow objektif-header border bg-white">
                                            <div class="row">
                                                <div class="col-12 col-md-8">
                                                    <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                                    <div>Sasaran: Jan 2018</div>
                                                    <div>Lampiran:</div>
                                                    <ul class="lampiran">
                                                        <li><a href="#"> Minit_1</a></li>
                                                        <li><a href="#"> Minit_2</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <div class="row justify-content-md-center">
                                                        <div class="col mt-2 mt-md-0">
                                                            <div>Kemajuan:</div>
                                                            <div class="kemajuan orange">
                                                                50%
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-grow switch-box">
                                            <div class="row">
                                                <div class="col align-self-center text-center">
                                                    <label class="switch">
                                                        <input type="checkbox">
                                                        <div class="switch-btn"></div>
                                                    </label>
                                                    <div class="switch-text">
                                                        <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        {{-- <div class="cards">
            <div> 
                <div class="container-fluid">
                    <div class="row">
                        <div class="col fokus-header">
                            <span>Fokus 1: Menyediakan Kemudahan Awam Dan Infrastruktur</span>
                        </div>
                        <div class="col-2 switch-box">
                            <div class="row">
                                <div class="col align-self-center text-center">
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                    <div class="switch-text">
                                        <u><a href="" data-toggle="modal" data-target="#commentModal">Komentar</a></u>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col objektif-header">
                            <span>Objektif 1: Mengurus Tadbir Urus Air</span>
                        </div>
                        <div class="col-2 switch-box">
                            <div class="row">
                                <div class="col align-self-center text-center">
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col parent-node  ml-0 ml-sm-4">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Lampiran:</div>
                                        <ul class="lampiran">
                                            <li><a href="#"> Minit_1</a></li>
                                            <li><a href="#"> Minit_2</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan orange">
                                                    50%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">
                                <div class="row">
                                    <div class="col align-self-center text-center">
                                        <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">
                                <div class="row">
                                    <div class="col align-self-center text-center">
                                        <label class="switch">
                                        <input type="checkbox" checked>
                                        <div class="switch-btn"></div>
                                    </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col parent-node  ml-0 ml-sm-4">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Lampiran:</div>
                                        <ul class="lampiran">
                                            <li><a href="#"> Minit_1</a></li>
                                            <li><a href="#"> Minit_2</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan orange">
                                                    50%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">
                                <div class="row">
                                    <div class="col align-self-center text-center">
                                        <label class="switch">
                                        <input type="checkbox" checked>
                                        <div class="switch-btn"></div>
                                    </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row inisiatif">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">
                                <div class="row">
                                    <div class="col align-self-center text-center">
                                        <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row inisiatif">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">
                                <div class="row">
                                    <div class="col align-self-center text-center">
                                        <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col parent-node  ml-0 ml-sm-4">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Lampiran:</div>
                                        <ul class="lampiran">
                                            <li><a href="#"> Minit_1</a></li>
                                            <li><a href="#"> Minit_2</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan orange">
                                                    50%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">
                                <div class="row">
                                    <div class="col align-self-center text-center">
                                        <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">
                                <div class="row">
                                    <div class="col align-self-center text-center">
                                        <label class="switch">
                                        <input type="checkbox">
                                        <div class="switch-btn"></div>
                                    </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            
            <div> 
                <div class="container-fluid">
                    <div class="row">
                        <div class="col fokus-header">
                            <span>Fokus 1: Menyediakan Kemudahan Awam Dan Infrastruktur</span>
                        </div>
                        <div class="col-2 switch-box">
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col objektif-header">
                            <span>Objektif 1: Mengurus Tadbir Urus Air</span>
                        </div>
                        <div class="col-2 switch-box">

                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col parent-node  ml-0 ml-sm-4">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Lampiran:</div>
                                        <ul class="lampiran">
                                            <li><a href="#"> Minit_1</a></li>
                                            <li><a href="#"> Minit_2</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan orange">
                                                    50%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">

                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col parent-node  ml-0 ml-sm-4">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Lampiran:</div>
                                        <ul class="lampiran">
                                            <li><a href="#"> Minit_1</a></li>
                                            <li><a href="#"> Minit_2</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan orange">
                                                    50%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">

                            </div>
                        </div>
                        <div class="row inisiatif">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">

                            </div>
                        </div>
                        <div class="row inisiatif">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">

                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col parent-node  ml-0 ml-sm-4">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>KPI 1: Jumplah Pembinaan Telaga Tiub</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Lampiran:</div>
                                        <ul class="lampiran">
                                            <li><a href="#"> Minit_1</a></li>
                                            <li><a href="#"> Minit_2</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan orange">
                                                    50%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col child-node ml-0 ml-sm-5">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div>Inisiatif 1-1: Mesyuarat/Perbincangan Awal</div>
                                        <div>Sasaran: Jan 2018</div>
                                        <div>Pencapaian: Selesai mesyuarat diadakan pada 9 Nov 2017</div>
                                        <div>Kemajuan: 100%</div>
                                        <div>Status: Aktif</div>
                                        <div>Catatan: Tiada catatan buat masa kini</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="row justify-content-md-center">
                                            <div class="col mt-2 mt-md-0">
                                                <div>Kemajuan:</div>
                                                <div class="kemajuan green">
                                                    100%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 switch-box">

                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div> --}}
        <div class="container-fluid">
            <div class="row">
                <div class="col text-sm-right p-0">
                    <button class="btn btn-primary mb-2">
                        <span><img class="btn-icon" src="../img/pdf-icon.svg" alt=""></span>
                        Eksport PDF
                    </button>
                    <button class="btn btn-primary mb-2">
                        <span><img class="btn-icon" src="../img/pdf-icon.svg" alt=""></span>
                        Pulang KPI
                    </button>
                    <button class="btn btn-primary mb-2">
                        <span><img class="btn-icon" src="../img/pdf-icon.svg" alt=""></span>
                        Serah KPI
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('modal.kembali')
@include('modal.komen')
@include('modal.simpan')
{{-- @include('modal.laporan-kpi') --}}
@include('modal.edit-kpi')
